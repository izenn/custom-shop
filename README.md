# Simple shop for custom NSP's and XCI's

parts of the code has been taken and adapted from:
* NSP-Indexer (https://gitlab.com/izenn/nsp-indexer)
* Resumable File Uploads With PHP & FlowJs (https://github.com/channaveer/php-flowjs-resumable-file-upload)
* Free Super Clean PHP File Directory Listing Script (https://github.com/halgatewood/file-directory-list)
