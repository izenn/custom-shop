<?php
$gameDir = "/config/www/repo";
$contentUrl = "/repo";
$allowedExtensions = explode(",", 'nsp,xci,nsz,xcz');

header("Content-Type: application/json");
header('Content-Disposition: filename="main.json"');
echo outputTinfoil();

function outputTinfoil()
{
    global $gameDir, $contentUrl;
    $output = array();
    $fileList = getFileList($gameDir);
    asort($fileList);
    $output["total"] = count($fileList);
    $output["files"] = array();
    $urlSchema = getURLSchema();
    foreach ($fileList as $file) {
		
		if (strtolower(PHP_SHLIB_SUFFIX) === 'dll'){
			$file = str_replace("\\\\","/",$file);
		}
		
		if(getenv('CUSTOM_EXTPORT')){
			if (!is_32bit()) {
				$output["files"][] = ['url' => $urlSchema . '://' . $_SERVER['SERVER_NAME']. ":". getenv('CUSTOM_EXTPORT') . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => getFileSize($gameDir . DIRECTORY_SEPARATOR . $file)];
			} else {
				$output["files"][] = ['url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . ":". getenv('CUSTOM_EXTPORT') . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => floatval(getFileSize($gameDir . DIRECTORY_SEPARATOR . $file))];
			}
		}else{	
			if (!is_32bit()) {
				$output["files"][] = ['url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => getFileSize($gameDir . DIRECTORY_SEPARATOR . $file)];
			} else {
				$output["files"][] = ['url' => $urlSchema . '://' . $_SERVER['SERVER_NAME'] . implode("/", array_map('rawurlencode', explode("/", $contentUrl . "/" . $file))), 'size' => floatval(getFileSize($gameDir . DIRECTORY_SEPARATOR . $file))];
			}
		}
    }
    return json_encode($output);
}


function getFileList($path)
{
    global $allowedExtensions;
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
    $arrFiles = array();
    foreach ($files as $file) {
        $parts = explode('.', $file);
        if (!$file->isDir() && in_array(strtolower(array_pop($parts)), $allowedExtensions)) {
			if (strtolower(PHP_SHLIB_SUFFIX) === 'dll'){
            array_push($arrFiles, str_replace("\\","\\\\",(str_replace($path . DIRECTORY_SEPARATOR, '', $file->getPathname()))));
			}else{
			array_push($arrFiles, str_replace($path . DIRECTORY_SEPARATOR, '', $file->getPathname()));
			}
        }
    }
    natcasesort($arrFiles);
    return array_values($arrFiles);
}

function is_32bit()
{
    return PHP_INT_SIZE === 4;
}

function getFileSize($filename)
{
    if (!is_32bit()) {
        $size = filesize($filename);
        return $size;
    }
    return getFileSize32bit($filename);
}

/* Hack for get filesize on big files > 4GB  on 32bit Systems */
function getFileSize32bit($filename)
{
    global $contentUrl;
    global $gameDir;
    $urlfilename = str_replace($gameDir, $contentUrl, $filename);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getURLSchema() . '://' . "127.0.0.1" . implode('/', array_map('rawurlencode', explode('/', $urlfilename))));
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    $data = curl_exec($ch);
    curl_close($ch);
    if ($data !== false && preg_match('/Content-Length: (\d+)/', $data, $matches)) {
        return $matches[1];
    }

}

function getURLSchema()
{
    $server_request_scheme = "http";
    if ((!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
        (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
        (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) {
        $server_request_scheme = 'https';
    }
    return $server_request_scheme;
}
